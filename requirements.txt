# This file may be used to create an environment using:
# $ conda create --name <env> --file <this file>
# platform: osx-64

Django==1.9.5
Pillow==3.2.0
psycopg2==2.6.1
numpy==1.11.1

#freetype==2.5.5
#h5py==2.6.0
#hdf5==1.8.17
#nltk==3.2.1
#openssl==1.0.2
#python-dateutil==2.5.3
requests==2.10.0
#setuptools==23.0.0
wheel==0.29.0
