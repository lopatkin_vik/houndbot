from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.views import generic
from .models import Choice, Question, Counter
from django.http import Http404
import json
import pprint
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.core.urlresolvers import reverse
from . import questionProcessor as qp
import requests



def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    context = {'latest_question_list': latest_question_list}
    return render(request, 'addOn/index.html', context)


def detail(request, question_id):
    try:
        question = Question.objects.get(pk=question_id)
    except Question.DoesNotExist:
        raise Http404("Question does not exist")
    return render(request, 'addOn/detail.html', {'question': question})


class Fbresponse(generic.View):
        def get(self, request, *args, **kwargs):
            if self.request.GET['hub.verify_token'] == '127664632':
                return HttpResponse(self.request.GET['hub.challenge'])
            else:
                return HttpResponse('Error, invalid token')

        @method_decorator(csrf_exempt)
        def dispatch(self, request, *args, **kwargs):
            return generic.View.dispatch(self, request, *args, **kwargs)

        # Post function to handle Facebook messages
        def post(self, request, *args, **kwargs):
            print("POST is called!!!!!")
            # Converts the text payload into a python dictionary
            incoming_message = json.loads(self.request.body.decode('utf-8'))
            # Facebook recommends going through every entry since they might send
            # multiple messages in a single call during high load


            for entry in incoming_message['entry']:
                #print (entry)

                for message in entry['messaging']:
                    # Check to make sure the received call is a message call
                    # This might be delivery, optin, postback for other events
                    if 'message' in message:
                        # Print the message to the terminal
                        print(message)
                        #post_facebook_message(message['sender']['id'], message['message']['text'])

                        qp.questionPull(message)
                    elif 'postback' in message:
                        qp.postbackHandler(message)
                        print("Postback message received")


            return HttpResponse()



from .models import Choice, Question
# ...
def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'addOn/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('addOn:results', args=(question.id,)))

def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'addOn/results.html', {'question': question})