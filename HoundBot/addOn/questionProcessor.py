import numpy
#import pandas
from collections import OrderedDict as od
from .models import Counter, Main_Profile, RE_Profile, Ins_Profile, Auto_Profile, FlowQuestions, Topic
from . import calculations
import json
import requests
import re
serviceWords=['help', 'options', 'stop', 'keywords']

questionList=od([(0,"Hello, let's start. What is the zipcode or city you are looking for a house in?"),
(1,"Ok, that's a nice area. What is the expected cost of the house?"),
(2,"How many years are you planning to live in this house?"),
(3,"Not bad, what is the downpayment on the house?"),
(4,"Have you checked with the bank? What is the expected interest rate you could get?"),
(5,"How much are HOAs"),
(6,"How much would you pay in homeowner's insurance? Market average is $100/mo"),
(7,"Based on your risk profile what is the expected market return rate on investment?"),
(8,"By how much has the value of real estate in this area grown each year?"),
(9,"What is the property tax rate?"),
(10,"This is the last answer")
]
)

profileFields=od([
(1,"zipcode"),
(2,"price"),
(3,"yrsStay"),
(4,"downpayment"),
(5,"loanrate"),
(6,"hoa"),
(7,"insurance"),
(8,"roiRt"),
(9,"appreciationRt"),
(10,"taxrate")
])

def post_facebook_message(response_msg):
    post_message_url = 'https://graph.facebook.com/v2.6/me/messages?access_token=EAACKU3jlWZCgBABrqePTqVuPFP25apubmqOYMMHB0GvPiZAnI1D3ddZB0IpEeounQ7UsCnWUTBCgBGAG7WFE3sORKpZC8VciaAD4l9NFIWDj2DSZBXqWODZCZCRz3pn0xtbxG2Y6qYjA8ZACZBwrqIt9wZCu7dVaRpw4P1VxnarqpHnwZDZD'
    status = requests.post(post_message_url, headers={"Content-Type": "application/json"},data=response_msg)
    #print(status.json())



def questionPull(message):

    try:
        if (message['sender']['id']!='833607546769144'):

            if any(message['message']['text'].lower() in s for s in serviceWords):
                serviceCall(message)
            else:
                messageHandler(message)

        else:
            response_msg = json.dumps({"recipient": {"id": '833607546769144'}, "message": {"text": "Empty"}})
            print("This is our message to client")
            post_facebook_message(response_msg)
    except:
        response_msg = json.dumps({"recipient": {"id": '833607546769144'}, "message": {"text": 'Empty'}})
        post_facebook_message(response_msg)
        print("Threw exception")

   # return response_msg



def messageHandler(message):
    fbid = message['sender']['id']

    print(Counter.objects.filter(userId=fbid))

    if Counter.filter(userId=fbid).count() > 0:
        counter = Counter.get(userId=fbid)
        profile = counter.re_profile_set.all()[0]
        currCount = counter.currentCount
    else:
        counter = Counter.objects.create(userId=fbid)
        profile = counter.re_profile_set.create(userId=fbid)
        currCount = counter.currentCount

    if 1 <= currCount <= max(profileFields.keys()):

        field = profileFields[currCount]
        try:

            valueNum=message['message']['text'].replace('%','')
            valueNum=valueNum.replace('$','')
            setattr(profile, field, valueNum)
            profile.save()
            print('Updating profile fields')
        except:
            print('Exception thrown while saving profile')

    try:
        if currCount == max(questionList.keys()):
            minRent = calculations.suggest(fbid)
            answer = 'Based on your answers we think that it is better to buy. However if you can find and apartment at below {} renting makes a lot of sense as well.'.format(
                minRent)
        else:
            answer = questionList[currCount]
        response_msg = json.dumps({"recipient": {"id": fbid}, "message": {"text": answer}})
        counter.currentCount += 1
        counter.save()
    except:
        if currCount == len(questionList):

            optionsCall(message,1)

            counter.currentCount = 0
            counter.save()


        else:
            answer = 'Looks like something went wrong. Try again later and I will try to fix myself.'
            response_msg = json.dumps({"recipient": {"id": fbid}, "message": {"text": answer}})
    #return response_msg
    post_facebook_message(response_msg)




def postbackHandler(message):
    fbid = message['sender']['id']
    payload=message['postback']['payload']
    print('Performing postback check')
    if payload =='start_over':
        response_msg = json.dumps({"recipient": {"id": fbid}, "message": {"text": "Alright, let's do it again"}})
        #return response_msg
        #views.post_facebook_message(response_msg)
        post_facebook_message(messageHandler(message))
    elif payload =='chat_with_expert':
        response_msg = json.dumps({"recipient": {"id": fbid}, "message": {"text": "Someone will contact you shortly."}})
        #return response_msg
        post_facebook_message(response_msg)
    elif payload == 'more_options':
        optionsCall(message,2)
    elif payload == 'buy_v_rent':
        print('Launch buy vs rent')
        #return messageHandler(message)
        post_facebook_message(messageHandler(message))
    elif payload=='help':
        message = json.dumps({"recipient": {"id": fbid}, "message": {"text": "help"}})
        serviceCall(message)
    elif payload=='options':
        message = json.dumps({"recipient": {"id": fbid}, "message": {"text": "options"}})
        serviceCall(message)
    elif payload == 'getstarted':
        response_msg1 = json.dumps({"recipient": {"id": fbid}, "message": {"text": "Welcome to the HoundBot. Let's start by running some simple commands and introducing you to the HoundBot capabilities."}})
        response_msg2 = json.dumps({"recipient": {"id": fbid}, "message": {
            "text": "Type 'help' to get instructions on common commands. Type 'options' to see all topics that the bot currently covers."}})
        Main_Profile.objects.create(userId=fbid)
        post_facebook_message(response_msg1)
        post_facebook_message(response_msg2)


def serviceCall(message):
    fbid = message['sender']['id']
    mtext=message['message']['text'].lower()
    counter = Counter.objects.get(userId=fbid)
    currCount = counter.currentCount
    if mtext=='help':
        response_msg = json.dumps({"recipient": {"id": fbid}, "message": {"text": "Happy to help. Here are some of the commands that will allow you to navigate HoundBot:\n  'options' - see available Q&A modules \n  'stop' - stop current Q&A session \n  'restart'- restart current session if you would like to provide new information \n  'keywords' - show all available keywords"}})

        #return response_msg
        post_facebook_message(response_msg)
    elif mtext=='stop':
        response_msg = json.dumps({"recipient": {"id": fbid}, "message": {
            "text": "Ok, we are stopping this session."}})

        #return response_msg
        post_facebook_message(response_msg)
        counter.currentCount = 0
        counter.save()
        print(counter.currentCount, currCount)
    elif mtext=='options':

        optionsCall(message,2)
    elif mtext=='keywords':
        response_msg=json.dumps({"recipient": {
                "id":fbid
              },
              "message":{
                "text":"Pick a keyword below",
                "quick_replies":[
                  {
                    "content_type":"text",
                    "title":"help",
                    "payload":"DEVELOPER_DEFINED_PAYLOAD_FOR_PICKING_RED"
                  },
                  {
                    "content_type":"text",
                    "title":"options",
                    "payload":"DEVELOPER_DEFINED_PAYLOAD_FOR_PICKING_GREEN"
                  },
                    {
                        "content_type": "text",
                        "title": "stop",
                        "payload": "DEVELOPER_DEFINED_PAYLOAD_FOR_PICKING_RED"
                    },
                    {
                        "content_type": "text",
                        "title": "start",
                        "payload": "DEVELOPER_DEFINED_PAYLOAD_FOR_PICKING_RED"
                    },
                    ]
                }
                    }
                    )
        #return response_msg
        post_facebook_message(response_msg)


def optionsCall(message, option):
    fbid = message['sender']['id']

    if option==1:

        response_msg = json.dumps(
            {

                "recipient": {

                    "id": fbid

                },

                "message": {

                    "attachment": {

                        "type": "template",

                        "payload": {

                            "template_type": "button",

                            "text": "What do you want to do next?",

                            "buttons": [

                                {

                                    "type": "postback",

                                    "title": "More options",

                                    "payload": "more_options"

                                },

                                {

                                    "type": "postback",

                                    "title": "Chat with expert",

                                    "payload": "chat_with_expert"

                                },
                                {

                                    "type": "postback",

                                    "title": "Start over",

                                    "payload": "start_over"

                                }

                            ]

                        }

                    }

                }

            }
        )
    else:
        response_msg = json.dumps(
            {
                "recipient": {
                    "id": fbid
                },
                "message": {
                    "attachment": {
                        "type": "template",
                        "payload": {
                            "template_type": "generic",
                            "elements": [
                                {
                                    "title": "Personal finance",
                                    #"image_url": "http://591d351d.ngrok.io/static/mark-and-john.gif",
                                    "image_url": "https://www.datazoom.co/static/rocket_launch.jpg",
                                    "subtitle": "Invest intelligently",
                                    "buttons": [
                                        {
                                            "type": "web_url",
                                            "url": "http://www.pacificexcel.com/",
                                            "title": "View partner website"
                                        },
                                        {
                                            "type": "postback",
                                            "title": "401(k) questions",
                                            "payload": "401k"
                                        },
                                        {
                                            "type": "postback",
                                            "title": "Personal investment",
                                            "payload": "pinv"
                                        },

                                    ]
                                },
                                {
                                    "title": "Real estate",
                                    #"image_url": "http://guillaumekurkdjian.com/wp-content/uploads/2015/10/maison_banlieue1.gif",
                                    "image_url": "https://www.datazoom.co/static/sf_houses.jpg",
                                    #"image_url": ,
                                    "subtitle": "Learn how to bring American dream to life",
                                    "buttons": [
                                        {
                                            "type": "postback",
                                            "title": "Buy vs Rent",
                                            "payload": "buy_v_rent"
                                        },
                                        {
                                            "type": "postback",
                                            "title": "What can I afford",
                                            "payload": "can_afford"
                                        },
                                        {
                                            "type": "postback",
                                            "title": "Explain how real estate investment works",
                                            "payload": "how_it_works"
                                        },

                                    ]
                                },
                                {
                                    "title": "Auto",
                                    "image_url": "https://www.datazoom.co/static/m3.jpg",
                                    "subtitle": "Let's talk cars",
                                    "buttons": [
                                        {
                                            "type": "postback",
                                            "title": "Buy vs Lease",
                                            "payload": "buy_v_lease"
                                        },
                                        {
                                            "type": "postback",
                                            "title": "Should I buy at all?",
                                            "payload": "buy_or_not"
                                        },
                                        {
                                            "type": "postback",
                                            "title": "Being scrappy",
                                            "payload": "scrappy"
                                        },

                                    ]
                                }
                            ]
                        }
                    }
                }

            }
        )

    #return response_msg
    post_facebook_message(response_msg)

