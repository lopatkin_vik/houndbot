import numpy as np
from .models import RE_Profile, Counter
from django.shortcuts import get_object_or_404
import locale

locale.setlocale( locale.LC_ALL, 'en_CA.UTF-8' )

def suggest(pk):
    profile = get_object_or_404(RE_Profile, userId=pk)
    price=profile.price
    downpayment=profile.downpayment
    loanInterest=profile.loanrate
    yrs=30
    hoa=profile.hoa
    principalPayoff=0.9
    insurance=profile.insurance
    taxRt=profile.taxrate
    yrsStay=profile.yrsStay
    aprnRate=profile.appreciationRt
    roiRt=profile.roiRt
    payment=-1*np.pmt(loanInterest/12,yrs*12,price-downpayment,0,1)
    tax=price*taxRt/12
    monthlyPayment=payment+hoa+insurance+tax
    closingCost=price*0.06/yrsStay
    appreciation=(price*(1+aprnRate)**(yrsStay))-price
    roi=(downpayment*(1+roiRt/12)**(yrsStay*12))-downpayment

    upsideOwn=payment*principalPayoff*12*yrsStay+(appreciation*(downpayment+(payment*principalPayoff*yrsStay*12))/price)-(payment*(1-principalPayoff)*12*yrsStay+closingCost*yrsStay+(hoa+insurance+tax)*12*yrsStay)
    suggestedRent=-1*(upsideOwn-roi)/(12*yrsStay)
    #suggestedRent=1600
    return locale.currency(suggestedRent, grouping=True)
