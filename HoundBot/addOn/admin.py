from django.contrib import admin

# Register your models here.
from .models import Choice, Question, Counter, RE_Profile, Main_Profile, Ins_Profile, Auto_Profile, Topic

class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 5

class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['question_text']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [ChoiceInline]

class CounterAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['question_text']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]

admin.site.register(Question, QuestionAdmin)
admin.site.register(Counter)
admin.site.register(RE_Profile)
admin.site.register(Ins_Profile)
admin.site.register(Auto_Profile)
admin.site.register(Topic)
admin.site.register(Main_Profile)