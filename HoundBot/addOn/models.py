from django.db import models
from django.contrib.postgres.fields import JSONField
# the following lines added:
import datetime
from django.utils import timezone

class Question(models.Model):
   question_text = models.CharField(max_length=200)
   pub_date = models.DateTimeField('date published')

   def __str__(self):
       return self.question_text

   def was_puplished_recently(self):
       now = timezone.now()
       return now - datetime.timedelta(days=1) <= self.pub_date <= now

   was_puplished_recently.admin_order_field = 'pub_date'
   was_puplished_recently.boolean = True
   was_puplished_recently.short_description = 'Published recently?'


class Choice(models.Model):
   question = models.ForeignKey(Question)
   choice_test = models.CharField(max_length=200)
   votes = models.IntegerField(default=0)

   def __str__(self):
       return self.choice_test




class Topic (models.Model):
    topicName = models.CharField(max_length=200)

class FlowQuestions (models.Model):
    topic = models.ForeignKey(Topic)
    sequenceId = models.IntegerField(default=0)
    questionText = models.CharField(max_length=500)
    followupprovisions = models.CharField(max_length=200)
    responsetype = models.IntegerField(default=0)
    nextquestionid = models.IntegerField(default=0)
    lastActive = models.BooleanField ()

class Main_Profile(models.Model):
    userId = models.CharField(max_length=200)
    firstname = models.CharField(max_length=200)
    secondname = models.CharField(max_length=200)
    lastchecked = models.DateTimeField()
    lasttopicid = models.IntegerField(default=0)
    lastquestionid = models.IntegerField(default=0)
    previousquestionid = models.IntegerField(default=0)


class Ins_Profile(models.Model):
    userId = models.CharField(max_length=200)
    zipcode = models.IntegerField(default=0)
    age = models.IntegerField(default=0)
    chronic_condition = models.BooleanField ()
    conditions = JSONField()
    dependent = models.BooleanField ()
    primaryCareChoice = models.BooleanField ()
    employed = models.BooleanField ()
    OOPCostPreference = models.IntegerField(default=0)
    frequentDoctorVisits = models.BooleanField ()
    frequentEmergencyCare = models.BooleanField ()
    exectedPregnancy = models.BooleanField ()
    expectedSurgery = models.BooleanField ()
    acuChiro = models.BooleanField ()
    mentalTreatment = models.BooleanField ()

class Auto_Profile(models.Model):
    carModel = models.CharField(max_length=200)
    year = models.IntegerField(default=0)
    price = models.IntegerField(default=0)
    downpayment = models.IntegerField(default=0)
    leaseDuration = models.IntegerField(default=0.0)
    annualMileage = models.IntegerField(default=0.0)
    commuterCar = models.BooleanField ()
    upgradeFrequency = models.IntegerField(default=0)
    depreciationRate = models.FloatField(default=0)
    serviceFeesYr = models.IntegerField(default=0.0)


class Counter(models.Model):
    userId = models.CharField(max_length=200)
    discoveryId=models.IntegerField(default=0)
    currentCount = models.IntegerField(default=0)

class RE_Profile(models.Model):
    counter=models.ForeignKey(Counter)
    userId = models.CharField(max_length=200)
    zipcode = models.IntegerField(default=0)
    price = models.IntegerField(default=0)
    downpayment = models.IntegerField(default=0)
    loanrate = models.FloatField(default=0.0)
    taxrate = models.FloatField(default=0.0)
    insurance = models.IntegerField(default=0)
    hoa = models.IntegerField(default=0)
    yrsStay = models.IntegerField(default=0)
    appreciationRt = models.FloatField(default=0.0)
    roiRt = models.FloatField(default=0.0)










