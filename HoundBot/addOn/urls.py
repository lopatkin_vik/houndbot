from django.conf.urls import url
from . import views

app_name='addOn'
urlpatterns=[
    url(r'^$', views.index, name='index'),
    url(r'^(?P<question_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^66d2b8f4a09cd35cb23076a1da5d51529136a3373fd570b122/?$', views.Fbresponse.as_view()),
    url(r'^(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),
    url(r'^(?P<question_id>[0-9]+)/results/$', views.results, name='results'),
    #url(r'^(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),
]
